<?php

function socialize_admin_settings($form, &$form_state) {
  $form['socialize'] = array(
    '#prefix' => t('Here you can access to configurations of all Social services define in this module.'),
    '#type'   => 'vertical_tabs',
  );

  $services = socialize_default_services();
  foreach ($services as $key => $service) {
    $form['socialize'][$key . '_wrapper'] = array(
      '#type'        => 'fieldset',
      '#title'       => $service['title'],
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );

    if (module_exists($service['module'])) {
      module_load_include('inc', 'socialize_twitter', 'includes/socialize_twitter.admin');
      $callback_form = $service['callback_form'];
      $form['socialize'][$key . '_wrapper'][$service['title']] = $callback_form($form, $form_state);

      // remove the vertical tab already existing
      if (isset($form['socialize'][$key . '_wrapper'][$service['title']]['socialize'])) {
        unset($form['socialize'][$key . '_wrapper'][$service['title']]['socialize']);
      }

      // remove action button
      if (isset($form['socialize'][$key . '_wrapper'][$service['title']]['actions']) &&
        isset($form['socialize'][$key . '_wrapper'][$service['title']]['#submit'])) {
        unset($form['socialize'][$key . '_wrapper'][$service['title']]['actions']);
        unset($form['socialize'][$key . '_wrapper'][$service['title']]['#submit']);
      }
    }
    else {
      $form['socialize'][$key . '_wrapper'][$service['title']] = array(
        '#prefix' => '<p>',
        '#markup' => t('Module <i>' . $service['title'] . '</i> (machine name: <i>' . $service['module'] . '</i>)' .
          ' is not installed or enabled. See ' . l('modules page', 'admin/modules') . ' to manage it.'),
        '#suffix' => '</p>',
      );
    }
  }

  return system_settings_form($form);
}
