<p>
  <?php foreach($entities as $tsid => $entity): ?>
  <div id="twitter-status-entity-<?php print $tsid; ?>" class="twitter-status-entity">
    <p>
      <?php print t('Posted by'); ?>
      <?php
        print l('@' . $entity['screen_name'], $twitter_host . '/' . $entity['screen_name'], array(
          'attributes' => array('target' => '_blank'),
        ));
      ?>
      -
      <?php print $entity['created_time']; ?>
    </p>

    <p><?php print $entity['text']; ?></p>

    <div class="twitter-status-entity-medias">
      <?php if (isset($entity['medias'])): ?>
        <?php foreach($entity['medias'] as $media): ?>
          <?php
            print theme('image', array(
              'path'       => $media['uri'],
              'width'      => '',
              'height'     => '100',
              'alt'        => '',
              'title'      => '',
              'attributes' => array(),
            ));
          ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
  <?php endforeach; ?>
</p>
