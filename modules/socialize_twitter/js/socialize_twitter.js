(function ($) {
  Drupal.behaviors.socialize_twitter = {
    attach: function(context) {
      $('.js-socialize-twitter', context).delegate(
        '.js-socialize-twitter__tweet',
        'click',
        this.import
      );
    },

    import: function() {
      var $this = $(this);
      var link_id = $this.attr('id');
      var id = link_id.replace('tweet_','');

      $.getJSON('/socialize_twitter/socialize/twitter/import/' + id, function(data) {
        $this.undelegate();
        $this.closest('.form-wrapper').css("background-color", "#e5ffe2");
        $this.parent('.import-link').html('<img src="/sites/all/modules/custom/socialize/images/watchdog-ok.png" />');
      });

      return false;
    }
  };
})(jQuery);
