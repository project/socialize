<?php

namespace Drupal\Socialize\Twitter;

class Twitter {

  const DEFAULT_TWITTER_HOST    ='https://twitter.com';
  const DEFAULT_TWITTER_API     ='https://dev.twitter.com';
  const DEFAULT_TWITTER_TINYURL ='http://tinyurl.com';

  /**
   * Codebird object
   *
   * @var \Codebird\Codebird
   */
  private $cb = NULL;

  /**
   * Constructor
   */
  public function __construct($consumer_key, $consumer_secret, $oauth_token, $oauth_token_secret) {
    \Codebird\Codebird::setConsumerKey($consumer_key, $consumer_secret);
    $this->cb = \Codebird\Codebird::getInstance();
    $this->cb->setToken($oauth_token, $oauth_token_secret);
  }

  /**
   * Get user settings
   *
   * @return array
   *  Settings
   */
  public function getUserSettings() {
    return (array) $this->cb->account_settings();
  }

  /**
   * Get user statuses
   *
   * @param array $params
   *  Filters params
   *
   * @return array
   *  List of tweets
   */
  public function getUserStatuses($params = array()) {
    return (array) $this->cb->statuses_userTimeline($params);
  }

  /**
   * Get status by id
   *
   * @param int $id
   *  Filters tweet id
   *
   * @return stdClass
   *  Tweet object
   */
  public function getStatusById($id) {
    return $this->cb->statuses_show_ID('id=' . $id);
  }

  /**
   * Post a new status
   *
   * @param array $params
   *  Post content
   */
  public function postStatus($status, $media_files = array()) {
    $params = array();
    $params['status'] = $status;

    if (!empty($media_files)) {
      $media_ids = array();
      foreach ($media_files as $file) {
        // upload all media files
        $reply = $this->cb->media_upload(array(
          'media' => $file
        ));
        // and collect their IDs
        $media_ids[] = $reply->media_id_string;
      }

      $media_ids = implode(',', $media_ids);
      $params['media_ids'] = $media_ids;
    }

    return $this->cb->statuses_update($params);
  }

}
