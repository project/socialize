<?php

class TwitterStatusController extends DrupalDefaultEntityController {

  /**
   * Create and return a new twitter_status entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'twitter_status';
    $entity->tsid = 0;
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity) {
    if (empty($entity->tsid)) {
      $entity->created = time();
    }

    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'twitter_status');
    $primary_keys = $entity->tsid ? 'tsid' : array();
    drupal_write_record('socialize_twitter_status', $entity, $primary_keys);
    $invocation = 'entity_insert';

    if (empty($primary_keys)) {
      field_attach_insert('twitter_status', $entity);
    }
    else {
      field_attach_update('twitter_status', $entity);
      $invocation = 'entity_update';
    }

    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'twitter_status');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more twitter_status entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param $twitter_status_ids
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($entities) {
    $twitter_status_ids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'twitter_status');
          field_attach_delete('twitter_status', $entity);
          $twitter_status_ids[] = $entity->tsid;
        }
        db_delete('socialize_twitter_status')->condition('tsid', $twitter_status_ids, 'IN')->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('twitter_status', $e);
        throw $e;
      }
    }
  }


  /**
   * Loads one or more twitter_status entities.
   *
   * @param $ids
   *   An array of entity IDs, or FALSE to load all entities.
   * @param $conditions
   *   An array of conditions in the form 'field' => $value.
   *
   * @return
   *   An array of entity objects indexed by their ids. When no results are
   *   found, an empty array is returned.
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = array();

    // Revisions are not statically cached, and require a different query to
    // other conditions, so separate the revision id into its own variable.
    if ($this->revisionKey && isset($conditions[$this->revisionKey])) {
      $revision_id = $conditions[$this->revisionKey];
      unset($conditions[$this->revisionKey]);
    }
    else {
      $revision_id = FALSE;
    }

    $passed_ids = !empty($ids) ? array_flip($ids) : FALSE;
    // Try to load entities from the static cache, if the entity type supports
    // static caching.
    if ($this->cache && !$revision_id) {
      $entities += $this->cacheGet($ids, $conditions);
      // If any entities were loaded, remove them from the ids still to load.
      if ($passed_ids) {
        $ids = array_keys(array_diff_key($passed_ids, $entities));
      }
    }

    // Load any remaining entities from the database. This is the case if $ids
    // is set to FALSE (so we load all entities), if there are any ids left to
    // load, if loading a revision, or if $conditions was passed without $ids.
    if ($ids === FALSE || empty($ids) || $ids || $revision_id || ($conditions && !$passed_ids)) {
      // Build the query.
      $query = $this->buildQuery($ids, $conditions, $revision_id);
      $queried_entities = $query->execute()->fetchAllAssoc($this->idKey);
    }

    // Pass all entities loaded from the database through $this->attachLoad(),
    // which attaches fields (if supported by the entity type) and calls the
    // entity type specific load callback, for example hook_node_load().
    if (!empty($queried_entities)) {
      $this->attachLoad($queried_entities, $revision_id);
      $entities += $queried_entities;
    }

    if ($this->cache) {
      // Add entities to the cache if we are not loading a revision.
      if (!empty($queried_entities) && !$revision_id) {
        $this->cacheSet($queried_entities);
      }
    }

    // Ensure that the returned array is ordered the same as the original
    // $ids array if this was passed in and remove any invalid ids.
    if ($passed_ids) {
      // Remove any invalid ids from the array.
      $passed_ids = array_intersect_key($passed_ids, $entities);
      foreach ($entities as $entity) {
        $passed_ids[$entity->{$this->idKey}] = $entity;
      }
      $entities = $passed_ids;
    }

    return $entities;
  }

}
