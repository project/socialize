<?php

use Drupal\Socialize\Twitter\Twitter;

/**
 * Implements hook_form().
 */
function socialize_twitter_admin_settings_form($form, &$form_state) {
  // Api keys
  $form['keys'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('API Keys'),
    '#description'   => t("Your application's API keys are used to authenticate requests to the Twitter Platform."),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['keys']['socialize_twitter_api_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('API Key'),
    '#default_value' => variable_get('socialize_twitter_api_key'),
    '#required'      => TRUE,
  );
  $form['keys']['socialize_twitter_api_secret'] = array(
    '#type'          => 'textfield',
    '#title'         => t('API Secret'),
    '#default_value' => variable_get('socialize_twitter_api_secret'),
    '#required'      => TRUE,
  );

  // Access token settings
  $form['keys_access_token'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Access token'),
    '#description'   => t("This access token can be used to make API requests on your own account's behalf. Do not share your access token secret with anyone."),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['keys_access_token']['socialize_twitter_access_token'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Access token'),
    '#default_value' => variable_get('socialize_twitter_access_token'),
    '#required'      => TRUE,
  );
  $form['keys_access_token']['socialize_twitter_access_token_secret'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Access token secret'),
    '#default_value' => variable_get('socialize_twitter_access_token_secret'),
    '#required'      => TRUE,
  );

  // Twitter external APIs settings
  $form['api'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Twitter API'),
    '#description'   => t('The following settings connect Twitter module with external APIs.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['api']['socialize_twitter_twitter_host'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Twitter host'),
    '#default_value' => variable_get('socialize_twitter_twitter_host', Twitter::DEFAULT_TWITTER_HOST),
    '#required'      => TRUE,
  );
  $form['api']['socialize_twitter_twitter_api'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Twitter API'),
    '#default_value' => variable_get('socialize_twitter_twitter_api', Twitter::DEFAULT_TWITTER_API),
    '#required'      => TRUE,
  );
  $form['api']['socialize_twitter_twitter_tinyurl'] = array(
    '#type'          => 'textfield',
    '#title'         => t('TinyURL'),
    '#default_value' => variable_get('socialize_twitter_twitter_tinyurl', Twitter::DEFAULT_TWITTER_TINYURL),
    '#required'      => TRUE,
  );

  // General setting
  $form['settings'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('General Configuration'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['settings']['socialize_twitter_count_tweet'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Max last tweets display'),
    '#description'   => t('Number of tweets display in the import list. Default: 50.'),
    '#default_value' => variable_get('socialize_twitter_count_tweet', 50),
    '#required'      => TRUE,
  );
  $form['settings']['socialize_twitter_auto_import'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable automatic import by a CRON task.'),
    '#default_value' => variable_get('socialize_twitter_auto_import', FALSE),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form().
 */
function socialize_twitter_admin_import_form($form, &$form_state) {
  module_load_include('inc', 'socialize_twitter', 'includes/socialize_twitter.model');
  drupal_add_js(drupal_get_path('module', 'socialize_twitter') . '/js/socialize_twitter.js', array('scope' => 'footer'));
  drupal_add_css(drupal_get_path('module', 'socialize_twitter') . '/css/socialize_twitter.css');

  $form['introduction'] = array(
    '#markup' => t('Visit ' . l(t('this page'), 'admin/structure/twitter_status/manage') . ' to see the complete Twitter Status list already imported.'),
  );

  $hide_retweeted = FALSE;
  if (isset($_SESSION['socialize_twitter_admin_import_form_filters'])) {
    $hide_retweeted = TRUE;
  }

  $list_twitter_status_imported = socialize_twitter_get_list_twitter_status_imported();
  if ($list_twitter_status_imported === FALSE) {
    $list_twitter_status_imported = array();
  }

  $auth_infos = array(
    'api_key'             => variable_get('socialize_twitter_api_key'),
    'api_secret'          => variable_get('socialize_twitter_api_secret'),
    'access_token'        => variable_get('socialize_twitter_access_token'),
    'access_token_secret' => variable_get('socialize_twitter_access_token_secret'),
  );
  $twitter = new Twitter($auth_infos['api_key'], $auth_infos['api_secret'], $auth_infos['access_token'], $auth_infos['access_token_secret']);

  $params['count'] = variable_get('socialize_twitter_count_tweet', 50);
  if ($hide_retweeted === TRUE) {
    $params['include_rts'] = FALSE;
  }
  $statuses = $twitter->getUserStatuses($params);

  // get only statuses not retweeted
  $original_statuses = array();
  foreach ($statuses as $status) {
    if (is_object($status)) {
      $original_statuses[$status->id] = $status;
    }
  }

  // filters form
  $form['filters'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Filters'),
    '#collapsible'   => FALSE,
  );
  $form['filters']['hide_retweeted'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Hide status retweeted'),
    '#default_value' => $hide_retweeted,
  );
  $form['filters']['actions'] = array(
    '#type'  => 'submit',
    '#value' => t('Filter'),
    '#name'  => 'filter',
  );

  foreach ($original_statuses as $id => $status) {
    $imported = FALSE;
    if (in_array($id, $list_twitter_status_imported)) {
      $imported = TRUE;
    }

    $retweeted = FALSE;
    if ($status->retweeted === TRUE) {
      $retweeted = TRUE;
    }

    if ($imported === FALSE) {
      $form['status-' . $id] = array(
        '#type'        => 'fieldset',
        '#title'       => $status->user->name . ' - ' . $status->created_at,
        '#collapsible' => FALSE,
        '#collapsed'   => FALSE,
        '#attributes'  => array(
          'class' => array('twitter-status-form-wrapper'),
        ),
      );
      $form['status-' . $id]['content'] = array(
        '#prefix'      => '<div class="clearfix"><div class="tweet-content">',
        '#markup'      => socialize_twitter_status_filters($status),
        '#suffix'      => '</div>',
      );
      $form['status-' . $id]['check'] = array(
        '#prefix'      => '<div class="import-link">',
        '#markup'      => l('Importer', '', array(
          'attributes' => array(
            'id'    => 'tweet_' . $id,
            'class' => array('js-socialize-twitter__tweet', 'button')
          ),
        )),
        '#suffix'      => '</div></div>',
      );
    }
  }

  $form['#attributes'] = array(
    'class' => 'js-socialize-twitter',
  );

  if (isset($_SESSION['socialize_twitter_admin_import_form_filters'])) {
    unset($_SESSION['socialize_twitter_admin_import_form_filters']);
  }

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function socialize_twitter_admin_import_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] === 'filter') {
    if ($form_state['values']['hide_retweeted'] === 1) {
      $_SESSION['socialize_twitter_admin_import_form_filters'] = array(
        'hide_retweeted' => TRUE,
      );
    }
    else {
      if (isset($_SESSION['socialize_twitter_admin_import_form_filters'])) {
        unset($_SESSION['socialize_twitter_admin_import_form_filters']);
      }
    }
  }
}

/**
 * Implements hook_form().
 */
function socialize_twitter_admin_post_form($form, &$form_state) {
  $form['twitter_status'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Publish a twitter status'),
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
  );
  $form['twitter_status']['text'] = array(
    '#type'        => 'textarea',
    '#title'       => t('Text'),
    '#maxlength'   => 140,
  );
  $form['twitter_status']['file'] = array(
    '#type'        => 'file',
    '#name'        => 'files[]',
    '#title'       => t('Upload photos'),
    '#description' => t('Allowed extensions: jpg, jpeg, gif, and png.'),
    '#attributes'  => array('multiple' => 'multiple'),
  );

  $form['twitter_status']['cancel'] = array(
    '#type'  => 'submit',
    '#name'  => 'cancel',
    '#value' => t('Cancel'),
  );
  $form['twitter_status']['submit'] = array(
    '#type'  => 'submit',
    '#name'  => 'publish',
    '#value' => t('Publish'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function socialize_twitter_admin_post_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] === 'publish') {
    if (empty($form_state['values']['text'])) {
      form_set_error('text', t('Text field is required.'));
    }

    //Save multiple files
    if (isset($_FILES['files']['name']) &&
      is_array($_FILES['files']['name']) &&
      !empty($_FILES['files']['name']) &&
      $_FILES['files']['name'][0] !== '') {

      foreach ($_FILES['files']['name'] as $key => $item) {
        $file = file_save_upload($key, array(
          'file_validate_is_image'   => array(),
          'file_validate_extensions' => array('png gif jpg jpeg'),
        ));

        if ($file) {
          $dest_dir = 'public://';
          if ($file = file_move($file, $dest_dir)) {
            $form_state['values']['file'][$key] = $file;
          }
          else {
            form_set_error('file', t('Failed to write the uploaded file in the file folder: ' . $dest_dir . '.'));
          }
        }
        else {
          form_set_error('file', t('No file was uploaded.'));
        }
      }
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function socialize_twitter_admin_post_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] === 'cancel') {
    $form_state['redirect'] = '<front>';
  }
  else if ($form_state['clicked_button']['#name'] === 'publish') {
    $attachments = array();

    $text = $form_state['values']['text'];

    if (is_array($form_state['values']['file'])) {
      foreach ($form_state['values']['file'] as $file) {
        $attachments[] = drupal_realpath($file->uri);
      }
    }

    // Post status
    $auth_infos = array(
      'api_key'             => variable_get('socialize_twitter_api_key'),
      'api_secret'          => variable_get('socialize_twitter_api_secret'),
      'access_token'        => variable_get('socialize_twitter_access_token'),
      'access_token_secret' => variable_get('socialize_twitter_access_token_secret'),
    );
    $twitter = new Twitter($auth_infos['api_key'], $auth_infos['api_secret'], $auth_infos['access_token'], $auth_infos['access_token_secret']);
    $reply = $twitter->postStatus($text, $attachments);

    if ($reply->httpstatus === 200) {
      // delete files saved
      if (is_array($form_state['values']['file'])) {
        foreach ($form_state['values']['file'] as $file) {
          file_delete($file, TRUE);
        }
      }

      // get current account
      $account = $twitter->getUserSettings();

      $twitter_host = variable_get('socialize_twitter_twitter_host', Twitter::DEFAULT_TWITTER_HOST);
      $twitter_account_path = $twitter_host . '/' . $account['screen_name'];
      $url = l($twitter_account_path, $twitter_account_path, array(
        'attributes' => array('target' => '_blank'),
      ));

      drupal_set_message(t("The twitter status has been published correctly. See " . $url));
    }
  }
}

/**
 * Form function to delete a twitter_status entity.
 */
function twitter_status_delete_form($form, &$form_state, $entity) {
  $form['description'] = array(
    '#prefix' => '<p>',
    '#markup' => t('This action cannot be undone.'),
    '#suffix' => '</p>',
  );

  $form['twitter_status'] = array(
    '#type'   => 'value',
    '#value'  => $entity,
  );

  field_attach_form('twitter_status', $entity, $form, $form_state);

  $form['delete'] = array(
    '#type'   => 'submit',
    '#value'  => t('Delete'),
    '#submit' => array('twitter_status_delete_entity'),
    '#weight' => 100,
  );
  $form['submit'] = array(
    '#markup' => l(t('Cancel'), 'admin/structure/twitter_status/manage'),
    '#weight' => 200,
  );

  return $form;
}

/**
 * Form deletion handler.
 */
function twitter_status_delete_entity($form , &$form_state) {
  module_load_include('inc', 'socialize_twitter', 'includes/socialize_twitter.model');
  $fids = array();

  $entity = $form_state['values']['twitter_status'];

  // delete files and medias
  $files = socialize_twitter_get_twitter_status_medias($entity);
  foreach ($files as $file) {
    $fids[] = $file->fid;
  }
  twitter_status_delete_medias($fids);

  twitter_status_delete($entity);

  drupal_set_message(t('The entity (ID %id) has been deleted', array(
    '%id' => $entity->tsid,
  )));

  $form_state['redirect'] = 'admin/structure/twitter_status/manage';
}
