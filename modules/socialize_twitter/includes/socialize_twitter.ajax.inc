<?php

use Drupal\Socialize\Twitter\Twitter;

function socialize_twitter_import_callback($id, $ajax_call = TRUE) {
  module_load_include('inc', 'socialize_twitter', 'includes/socialize_twitter.model');

  $twitter_status = socialize_twitter_get_twitter_status_by_id($id);
  if ($twitter_status === FALSE) {
    $auth_infos = array(
      'api_key'             => variable_get('socialize_twitter_api_key'),
      'api_secret'          => variable_get('socialize_twitter_api_secret'),
      'access_token'        => variable_get('socialize_twitter_access_token'),
      'access_token_secret' => variable_get('socialize_twitter_access_token_secret'),
    );
    $twitter = new Twitter($auth_infos['api_key'], $auth_infos['api_secret'], $auth_infos['access_token'], $auth_infos['access_token_secret']);
    $status = $twitter->getStatusById($id);

    $twitter_status = socialize_twitter_save_twitter_status($status);
  }

  $out['twitter_status'] = $twitter_status;

  if ($ajax_call === TRUE) {
    drupal_json_output($out);
  }
  else {
    return $out;
  }
}
