<?php

/**
 * Get status by Twitter status id
 *
 * @param string $id
 *  The Twitter status id
 *
 * @return stdClass $out
 *  Twitter status object if exists, False otherwise
 */
function socialize_twitter_get_twitter_status_by_id($id) {
  $out = FALSE;

  $query = db_select('socialize_twitter_status', 'sts');
  $query->fields('sts');
  $query->condition('sts.twitter_status_id', $id);

  $result = $query->execute()->fetchAssoc();
  if (isset($result) && !empty($result)) {
    $out = $result;
  }

  return $out;
}


/**
 * Get Twitter status list imported
 *
 * @return array $out
 *  List array if exists, False otherwise
 */
function socialize_twitter_get_list_twitter_status_imported() {
  $out = FALSE;

  $query = db_select('socialize_twitter_status', 'sts');
  $query->fields('sts', array('tsid', 'twitter_status_id'));
  $result = $query->execute()->fetchAllKeyed(0, 1);
  if (isset($result) && !empty($result)) {
    $out = $result;
  }

  return $out;
}

/**
 * Save a new twitter status in database
 *
 * @param stdClass $status
 *  A status object
 */
function socialize_twitter_save_twitter_status($status) {
  $twitter_status = FALSE;

//   krumo($status);
//   die;

  if (isset($status) && is_object($status)) {
    $date = new DateTime($status->created_at);
    $timestamp_date = $date->getTimestamp();

    // apply filters on status text
    $text = socialize_twitter_status_filters($status);

    $twitter_status = new stdClass();
    $twitter_status->twitter_status_id       = $status->id;
    $twitter_status->screen_name             = $status->user->screen_name;
    $twitter_status->created_at              = $status->created_at;
    $twitter_status->created_time            = $timestamp_date;
    $twitter_status->text                    = $text;
    $twitter_status->source                  = $status->source;
    $twitter_status->in_reply_to_status_id   = $status->in_reply_to_status_id;
    $twitter_status->in_reply_to_user_id     = $status->in_reply_to_user_id;
    $twitter_status->in_reply_to_screen_name = $status->in_reply_to_screen_name;
    $twitter_status->truncated               = $status->truncated;

    // save the status entity in DB
    $twitter_status = twitter_status_save($twitter_status);

    // save the status medias
    if (isset($status->extended_entities) && isset($status->extended_entities->media) && isset($twitter_status->tsid)) {
      $medias = $status->extended_entities->media;
      if (!empty($medias)) {
        foreach ($medias as $media) {
          // save file
          $file = twitter_status_save_file($media->media_url);
          if (isset($file->fid)) {
            // save media
            twitter_status_save_media($media->id, $media->type, $twitter_status->tsid, $file->fid);
          }
        }
      }
    }
  }

  return $twitter_status;
}

/**
 * Apply filters on text
 *
 * @param stdClass $entity
 *  The twitter status entity
 *
 * @return string $text
 *  The string filtered
 */
function socialize_twitter_status_filters($entity) {
$text = $entity->text;

  if (isset($entity->entities) && !empty($entity->entities)) {
    $list_entities = (array) $entity->entities;

    foreach ($list_entities as $entity_name => $entities) {
      switch ($entity_name) {
        case 'hashtags':
          $text = _socialize_twitter_status_filters_hashtag($text, $entities);
          break;

        case 'symbols':
          break;

        case 'urls':
          $text = _socialize_twitter_status_filters_link($text, $entities);
          break;

        case 'user_mentions':
          $text = _socialize_twitter_status_filters_username($text, $entities);
          break;
      }
    }
  }

  return _socialize_twitter_status_filters_url($text);
}

/**
 * Save file in database.
 *
 * @param string $uri
 *  The ressource uri to the media
 *
 * @return stdClass $file
 *  The updated file object.
 */
function twitter_status_save_file($uri) {
  $file = new stdClass();
  $file->uid         = 0; // $user->uid;
  $file->filename    = $uri;
  $file->uri         = $uri;
  $file->filemime    = file_get_mimetype($uri);
  $file->filesize    = filesize($uri);
  $file->status      = FILE_STATUS_PERMANENT;
  $file->timestamp   = time();
  $file->display     = '1';
  $file->description = '';

  return file_save($file);
}

/**
 * Save the media for the twitter status corresponding.
 *
 * @param int $id
 *  The twitter status media id
 *
 * @param string $type
 *  The twitter status media type
 *
 * @param int $tsid
 *  The twitter status primary key id
 *
 * @param int $fid
 *  The twitter status media file id
 */
function twitter_status_save_media($id, $type, $tsid, $fid) {
  db_insert('socialize_twitter_status_media')
    ->fields(array(
      'twitter_status_media_id' => $id,
      'type'                    => $type,
      'tsid'                    => $tsid,
      'fid'                     => $fid,
    ))
    ->execute();
}

/**
 * Get Twitter status media
 *
 * @param stdClass $entity
 *  The Twitter status entity (optional)
 *
 * @return array $out
 *  Twitter status media object if exists, False otherwise.
 *  If parameter is null, return the full media list.
 */
function socialize_twitter_get_twitter_status_medias($entity = NULL) {
  $out = FALSE;

  $query = db_select('socialize_twitter_status_media', 'stsm');
  $query->fields('stsm', array('tsmid', 'fid'));

  if ($entity !== NULL) {
    $query->condition('stsm.tsid', $entity->tsid);
  }

  $result = $query->execute()->fetchAllKeyed(0, 1);
  if (isset($result) && !empty($result)) {
    $files = $result;
    if ($entity !== NULL) {
      $files = file_load_multiple($result);
    }
    $out = $files;
  }

  return $out;
}

/**
 * Get the full twitter status entities list.
 */
function twitter_status_get_full_entities() {
  $out = array();

  $query = db_select('socialize_twitter_status', 'sts');
  $query->fields('sts');
  $query->fields('stsm', array('twitter_status_media_id'));
  $query->fields('fm', array('fid', 'uri'));
  $query->leftJoin('socialize_twitter_status_media', 'stsm', 'sts.tsid = stsm.tsid');
  $query->leftJoin('file_managed', 'fm', 'fm.fid = stsm.fid');
  $result = $query->execute()->fetchAll();

  if (isset($result) && !empty($result)) {
    foreach ($result as $entity) {
      $out[$entity->tsid]['screen_name']             = $entity->screen_name;
      $out[$entity->tsid]['created_time']            = format_date($entity->created_time, 'custom', 'd/m/Y');
      $out[$entity->tsid]['text']                    = $entity->text;
      $out[$entity->tsid]['in_reply_to_screen_name'] = $entity->in_reply_to_screen_name;
      if (isset($entity->fid) && isset($entity->uri)) {
        $out[$entity->tsid]['medias'][] = array(
          'fid' => $entity->fid,
          'uri' => $entity->uri,
        );
      }
    }
  }

  return $out;
}

/**
 * Delete medias
 *
 * @param array $fids
 *  List of files id
 */
function twitter_status_delete_medias($fids = array()) {
  if (!empty($fids)) {
    db_delete('socialize_twitter_status_media')->condition('fid', $fids)->execute();
    db_delete('file_managed')->condition('fid', $fids)->execute();
  }
}
